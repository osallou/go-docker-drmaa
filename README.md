# DRMAA

DRMAA v1 implementation for Go-Docker


## Dependencies

Jansson, curl libraries (and devel ones for compilation)

Debian: libjansson-dev, libcurl4-nss-dev

## Compilation

cmake .
make

## Running

To authenticate, one must create a file containing godocker identifiers

    my_login
    my_api_key
    https://godocker.genouest.org


and set it in a environement variable

    export GODAUTH=my_auth_file_path


In native specifications, it is possible to specify some attributes, separated by a comma.

    cpu:2,ram:6,image:centos,volume:home,volume:db,project:default

Note that image cannot contain tags (using latest), and that volumes are mounted by default in rw mode (if allowed)

## Debug

drmaa_test: sample use case test
test.py: Python drmaa (need drmaa module) sample test

valgrind --tool=memcheck --leak-check=full --log-file="valgrind.out" ./drmaa_test


## Not yet implemented methods

int drmaa_get_attribute(drmaa_job_template_t *jt, const char *name, char *value,
                        size_t value_len, char *error_diagnosis,
                        size_t error_diag_len)

int drmaa_get_next_attr_name(drmaa_attr_names_t* values, char *value,
                             size_t value_len)

int drmaa_get_next_attr_value(drmaa_attr_values_t* values, char *value,
                              size_t value_len)

int drmaa_get_next_job_id(drmaa_job_ids_t* values, char *value,
                          size_t value_len)

int drmaa_get_num_attr_names(drmaa_attr_names_t* values, int *size)

int drmaa_get_num_attr_values(drmaa_attr_values_t* values, int *size)

int drmaa_get_num_job_ids(drmaa_job_ids_t* values, int *size)

void drmaa_release_attr_names(drmaa_attr_names_t* values)

void drmaa_release_attr_values(drmaa_attr_values_t* values)

void drmaa_release_job_ids(drmaa_job_ids_t* values)

int drmaa_get_vector_attribute(drmaa_job_template_t *jt, const char *name,
                               drmaa_attr_values_t **values,
                               char *error_diagnosis, size_t error_diag_len)

int drmaa_get_attribute_names(drmaa_attr_names_t **values,
                              char *error_diagnosis, size_t error_diag_len)


int drmaa_get_vector_attribute_names(drmaa_attr_names_t **values,
                                     char *error_diagnosis,
                                     size_t error_diag_len)

int drmaa_run_bulk_jobs(drmaa_job_ids_t **jobids,
                        const drmaa_job_template_t *jt, int start, int end,
                        int incr, char *error_diagnosis, size_t error_diag_len)

int drmaa_control(const char *jobid, int action, char *error_diagnosis,
                  size_t error_diag_len)

import drmaa
import os

def main():
   """
   Submit a job.
   Note, need file called sleeper.sh in current directory.
   """
   with drmaa.Session() as s:
       print('Creating job template')
       jt = s.createJobTemplate()
       jt.remoteCommand = 'echo'
       jt.args = ['hello', 'world']
       jt.joinFiles=True
       jt.nativeSpecification = 'cpu:2,image:centos,volume:home'

       jobid = s.runJob(jt)
       print('Your job has been submitted with ID %s' % jobid)

       print('Cleaning up')
       s.deleteJobTemplate(jt)

if __name__=='__main__':
   main()

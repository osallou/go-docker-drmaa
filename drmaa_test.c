#include <stdio.h>
#include <stdlib.h>
#include "drmaa.h"


int main()
{
    printf("DRMAA TEST\n");
    char error[DRMAA_ERROR_STRING_BUFFER];
    int errnum = 0;
    errnum = drmaa_init (NULL, error, DRMAA_ERROR_STRING_BUFFER);
    char* god_auth = getenv("GODAUTH");
    if (errnum != DRMAA_ERRNO_SUCCESS) {
        fprintf (stderr, "Could not initialize the DRMAA library: %s\n", error);
        return 1;
    }

    printf ("DRMAA library was started successfully\n");

    unsigned int major;
    unsigned int minor;
    errnum = drmaa_version(&major, &minor, error, DRMAA_ERROR_STRING_BUFFER);
    printf("Version: %d.%d\n", major, minor);

    char contact[DRMAA_CONTACT_BUFFER];
    errnum = drmaa_get_contact(contact, DRMAA_CONTACT_BUFFER, error, DRMAA_ERROR_STRING_BUFFER);
    printf("Contact: %s\n",contact);


    // Create job template
    drmaa_job_template_t *jt = NULL;
    errnum = drmaa_allocate_job_template (&jt, error, DRMAA_ERROR_STRING_BUFFER);
    if (errnum != DRMAA_ERRNO_SUCCESS) {
       fprintf (stderr, "Could not create job template: %s\n", error);
       return 0;
    }
    errnum = drmaa_set_attribute (jt, DRMAA_REMOTE_COMMAND, "echo",
                                    error, DRMAA_ERROR_STRING_BUFFER);
    const char *args[3] = {"5", "douze", NULL};
    errnum = drmaa_set_vector_attribute (jt, DRMAA_V_ARGV, args, error,
                                              DRMAA_ERROR_STRING_BUFFER);

    // Set native specs, allowed are cpu and ram
    errnum = drmaa_set_attribute (jt, DRMAA_NATIVE_SPECIFICATION, "cpu:2,ram:4,image:centos,volume:home,volume:db",
                                    error, DRMAA_ERROR_STRING_BUFFER);

    char jobid[DRMAA_JOBNAME_BUFFER];

    errnum = drmaa_run_job (jobid, DRMAA_JOBNAME_BUFFER, jt, error,
                                  DRMAA_ERROR_STRING_BUFFER);

    if (errnum != DRMAA_ERRNO_SUCCESS) {
            fprintf (stderr, "Could not submit job: %s\n", error);
    }
    else {
            printf ("Your job has been submitted with id %s\n", jobid);
    }
    char jobid_out[DRMAA_JOBNAME_BUFFER];
    int status = 0;
    drmaa_attr_values_t *rusage = NULL;

    int remote_ps = -1;

    errnum = drmaa_job_ps(jobid, &remote_ps,error,
                     DRMAA_ERROR_STRING_BUFFER);

    printf ("remote_ps %d\n", remote_ps);
    /*
    errnum = drmaa_wait (jobid, jobid_out, DRMAA_JOBNAME_BUFFER, &status,
                                  DRMAA_TIMEOUT_WAIT_FOREVER, &rusage, error,
                                 DRMAA_ERROR_STRING_BUFFER);
    */
    const char* jobids[] = {DRMAA_JOB_IDS_SESSION_ALL, NULL};
    errnum = drmaa_synchronize(jobids, DRMAA_TIMEOUT_WAIT_FOREVER, 0,
                         error, DRMAA_ERROR_STRING_BUFFER);

    printf ("drmaa_wait status %d\n", status);
    int exit_status = -1;
    errnum = drmaa_wexitstatus(&exit_status, status, error,
                                                      DRMAA_ERROR_STRING_BUFFER);

    printf ("Your job exited with code %d\n", exit_status);
    errnum = drmaa_delete_job_template (jt, error, DRMAA_ERROR_STRING_BUFFER);

    errnum = drmaa_exit (error, DRMAA_ERROR_STRING_BUFFER);

    if (errnum != DRMAA_ERRNO_SUCCESS) {
        fprintf (stderr, "Could not shut down the DRMAA library: %s\n", error);
        return 1;
    }

    return 0;
}

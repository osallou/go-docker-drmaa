#include <stdio.h>
#include <stdlib.h>
#include "drmaa.h"
#include <jansson.h>
#include <curl/curl.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include "GoDockerDrmaa.h"
#include "vector.h"

void vector_init(Vector *vector) {
  // initialize size and capacity
  vector->size = 0;
  vector->capacity = VECTOR_INITIAL_CAPACITY;

  // allocate memory for vector->data
  vector->data = malloc(sizeof(int) * vector->capacity);
}

void vector_append(Vector *vector, int value) {
  // make sure there's room to expand into
  vector_double_capacity_if_full(vector);

  // append the value and increment vector->size
  vector->data[vector->size++] = value;
}

int vector_get(Vector *vector, int index) {
  if (index >= vector->size || index < 0) {
    printf("Index %d out of bounds for vector of size %d\n", index, vector->size);
    exit(1);
  }
  return vector->data[index];
}

void vector_set(Vector *vector, int index, int value) {
  // zero fill the vector up to the desired index
  while (index >= vector->size) {
    vector_append(vector, 0);
  }

  // set the value at the desired index
  vector->data[index] = value;
}

void vector_double_capacity_if_full(Vector *vector) {
  if (vector->size >= vector->capacity) {
    // double vector->capacity and resize the allocated memory accordingly
    vector->capacity *= 2;
    vector->data = realloc(vector->data, sizeof(int) * vector->capacity);
  }
}

void vector_free(Vector *vector) {
  free(vector->data);
}

void vector_clear(Vector *vector) {
  vector->size = 0;
  free(vector->data);
  vector->data = malloc(sizeof(int) * vector->capacity);
}

Vector job_vector;



json_t* config = NULL;



int test()
{
  printf("Hello world\n");
  char* god_auth = getenv("GODAUTH");

  return 0;
}

int curl_call(void)
{
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "http://example.com");
    /* example.com is redirected, so we tell libcurl to follow redirection */
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));

    /* always cleanup */
    curl_easy_cleanup(curl);
  }
  return 0;
}

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}

char* get_token(const char *uid, const char *apikey, const char *server) {
    CURL *curl;
    CURLcode res;
    FILE *pagefile;
    const char* token_string;
    char* token;
    //static const char *pagefilename = "page.out";
    static char template[] = "/tmp/godockerXXXXXX";
	char pagefilename[PATH_MAX];
    strcpy(pagefilename, template);
	mktemp(pagefilename);

    //printf("TEMP %s\n",pagefilename);

    int newSize = strlen(server) + strlen("/api/1.0/authenticate") + 1;
    char* api_auth = (char *)malloc(newSize);
    strcpy(api_auth, server);
    strcat(api_auth, "/api/1.0/authenticate");
    curl = curl_easy_init();
    if(curl) {
      fprintf(stdout, "API Call: %s\n", api_auth);
      curl_easy_setopt(curl, CURLOPT_URL, api_auth);
      json_t *json_auth = json_pack("{ssss}", "apikey", apikey, "user", uid);
      if(json_auth == NULL) {
          free(api_auth);
          return NULL;
      }
      char *json_auth_str = json_dumps(json_auth, 0);

      struct curl_slist *headers=NULL;
      headers = curl_slist_append(headers, "Content-Type: application/json");
      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_auth_str);
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
      pagefile = fopen(pagefilename, "wb");
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, pagefile);
      /* Perform the request, res will get the return code */
      curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
      res = curl_easy_perform(curl);
      fclose(pagefile);
      /* Check for errors */
      if(res != CURLE_OK)
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
      /* always cleanup */
      curl_slist_free_all(headers);
      curl_easy_cleanup(curl);
      free(json_auth_str);
      json_decref(json_auth);

      json_t *json;
      json_error_t error;
      json = json_load_file(pagefilename, 0, &error);
      if(json) {
          json_t *res = json_object_get(json, "token");
          token_string = json_string_value(res);
          token = malloc(strlen(token_string)+1);
          strcpy(token, token_string);
          json_object_clear(json);
          //json_decref(res);
          json_decref(json);
      }
      else {
          token_string = NULL;
          token = NULL;
      }
      unlink(pagefilename);
    }
    free(api_auth);
    //return token_string;
    return token;
}

void show_json(json_t *obj){
    if(obj) {
    char * job_json = json_dumps(obj, 0);
    fprintf(stdout,"%s\n", job_json);
    free(job_json);
    }
}

char *choppy( char *s )
{
    char *n = malloc( strlen( s ? s : "\n" ) +1);
    if( s )
        strcpy( n, s );
    n[strlen(n)-1]='\0';
    free(s);
    return n;
}

char* authenticate(char *error_diagnosis, size_t error_diag_len, json_t* job) {
    FILE * fp;
    char * uid = NULL;
    char * apikey = NULL;
    char* server = NULL;
    size_t len = 0;
    ssize_t read;
    char* god_auth = getenv("GODAUTH");

    fp = fopen(god_auth, "r");
    if (fp == NULL) {
        //error_diag_len = strlen(NOAUTH);
        //strncat(error_diagnosis, NOAUTH, error_diag_len);
        strcpy(error_diagnosis, NOAUTH);
        return NULL;
    }
    // Login
    read = getline(&uid, &len, fp);
    if(read == -1){
        error_diag_len = strlen(NOAUTH_LOGIN);
        strncat(error_diagnosis, NOAUTH_LOGIN, error_diag_len);
        return NULL;
    }
    uid = choppy(uid);

    read = getline(&apikey, &len, fp);
    if(read == -1){
        error_diag_len = strlen(NOAUTH_API);
        strncat(error_diagnosis, NOAUTH_API, error_diag_len);
        return NULL;
    }
    apikey = choppy(apikey);

    read = getline(&server, &len, fp);
    if(read == -1){
        error_diag_len = strlen(NOAUTH_SERVER);
        strncat(error_diagnosis, NOAUTH_SERVER, error_diag_len);
        return NULL;
    }
    server = choppy(server);
    fclose(fp);

    char* token = get_token(uid, apikey, server);
    if(token == NULL){
        error_diag_len = strlen(NOAUTH_ERROR);
        strncat(error_diagnosis, NOAUTH_ERROR, error_diag_len);
        free(uid);
        free(apikey);
        free(server);
        return NULL;
    }
    else {
        json_t* jtoken = json_string(token);
        json_t* jserver = json_string(server);
        if(job != NULL) {
            json_object_set(job, "god_token", jtoken);
            json_object_set(job, "god_server",jserver);
        }
        if(config == NULL) {
            json_error_t error;
            config = json_loads("{\"god_server\":\"\",\"god_token\":\"\"}", 0, &error );
            json_object_set(config,"god_server", jserver);
            json_object_set(config,"god_token", jtoken);
        }
        json_decref(jtoken);
        json_decref(jserver);
    }
    if (uid) {
        free(uid);
    }
    if (apikey) {
        free(apikey);
    }
    if (server) {
        free(server);
    }
    return token;
}

json_t* api_call(json_t* input, const char* url, const char* method) {
    json_t* token = json_object_get(config, "god_token");
    if(token == NULL) {
        char error[DRMAA_ERROR_STRING_BUFFER];
        char* err = authenticate(error, DRMAA_ERROR_STRING_BUFFER, input);
    }

    const json_t* token_json = json_object_get(config, "god_token");
    const json_t* server_json = json_object_get(config, "god_server");
    const char* token_string = json_string_value(token_json);
    const char* server = json_string_value(server_json);

    CURL *curl;
    CURLcode res;
    FILE *pagefile;
    //static const char *pagefilename = "page.out";
    static char template[] = "/tmp/godockerXXXXXX";
    char pagefilename[PATH_MAX];
    strcpy(pagefilename, template);
    mktemp(pagefilename);

    char *json_input_str = NULL;

    json_t *json;
    json_error_t error;

    int newSize = strlen(server) + strlen(url) + 1;
    char* api_auth = (char *)malloc(newSize);;
    strcpy(api_auth, server);
    strcat(api_auth, url);
    curl = curl_easy_init();
    if(curl) {
      fprintf(stdout, "API Call: %s\n", api_auth);

      curl_easy_setopt(curl, CURLOPT_URL, api_auth);
      char* token_bearer = malloc(strlen("Authorization:Bearer ")+strlen(token_string) + 1);
      strcpy(token_bearer, "Authorization:Bearer ");
      strcat(token_bearer, token_string);

      struct curl_slist *headers=NULL;
      headers = curl_slist_append(headers, "Content-Type: application/json");
      headers = curl_slist_append(headers, token_bearer);
      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
      if(strcmp(method,"POST") == 0 && input) {
          json_input_str = json_dumps(input, 0);
          curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_input_str);
      }
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
      pagefile = fopen(pagefilename, "wb");
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, pagefile);
      /* Perform the request, res will get the return code */
      curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
      res = curl_easy_perform(curl);
      fclose(pagefile);
      /* Check for errors */
      if(res != CURLE_OK)
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
        // Auth token expired? unset it, next call will reauth
        json_object_del(config, "god_token"); 
      /* always cleanup */
      curl_slist_free_all(headers);
      curl_easy_cleanup(curl);
      if(json_input_str != NULL){
          free(json_input_str);
      }
      json = json_load_file(pagefilename, 0, &error);
      if(json == NULL) {
          fprintf(stderr, "%d:%s", error.line, error.text);
      }
      unlink(pagefilename);
      free(token_bearer);
    }
    free(api_auth);

    return json;
}

json_t* api_post(json_t* input, const char* url) {
    return api_call(input, url, "POST");
}

json_t* api_get(json_t* input, const char* url) {
    return api_call(input, url, "GET");
}


int drmaa_get_contact(char *contact, size_t contact_len,
         char *error_diagnosis, size_t error_diag_len) {
             //contact = malloc(sizeof(CONTACT));
             strcpy(contact, CONTACT);
         }

int drmaa_version(unsigned int *major, unsigned int *minor,
         char *error_diagnosis, size_t error_diag_len) {

             *major = drmaa_VERSION_MAJOR;
             *minor = drmaa_VERSION_MINOR;

             return DRMAA_ERRNO_SUCCESS;
         }


int drmaa_init(const char *contact, char *error_diagnosis,
               size_t error_diag_len) {
    vector_init(&job_vector);
    char* token = authenticate(error_diagnosis, error_diag_len, NULL);
    if(token != NULL) {
        free(token);
        return DRMAA_ERRNO_SUCCESS;
    }
    else {
        return DRMAA_ERRNO_INTERNAL_ERROR;
    }

}

int drmaa_allocate_job_template(drmaa_job_template_t **jt,
                                char *error_diagnosis, size_t error_diag_len) {
     json_error_t error;
     //fprintf(stdout,"%s\n", basetask);
     *jt = (drmaa_job_template_t *)malloc(sizeof(drmaa_job_template_t));
     (*jt)->json = json_loads(basetask, 0, &error );
     if(! (*jt)->json) {
         fprintf(stderr, "%d:%s\n",error.line, error.text);
     }
     return DRMAA_ERRNO_SUCCESS;
}

void set_native(json_t* jt, char* token) {
  const char s[2] = ":";
  char *saveptr;
  char *key = strtok_r(token, s, &saveptr);
  char *value = strtok_r(NULL, s, &saveptr);
  json_t *requirements = json_object_get(jt, "requirements");
  json_t *container = json_object_get(jt, "container");
  json_t *volumes = json_object_get(container, "volumes");
  json_t *user = json_object_get(jt, "user");

  //fprintf(stdout,"#%s\n",key);
  if(strcmp(key,"cpu") == 0) {
      json_t *jvalue = json_integer(atoi(value));
      json_object_set(requirements, "cpu", jvalue);
      json_decref(jvalue);
  }
  else if(strcmp(key,"ram") == 0) {
      json_t *jvalue = json_integer(atoi(value));
      json_object_set(requirements, "ram", jvalue);
      json_decref(jvalue);
  }
  else if(strcmp(key,"image") == 0) {
      json_t *jvalue = json_string(value);
      json_object_set(container, "image", jvalue);
      json_decref(jvalue);
  }
  else if(strcmp(key,"volume") == 0) {
      json_t *volume = json_object();
      json_t *jvalue = json_string(value);
      json_t *jacl = json_string("rw");
      json_object_set(volume, "name", jvalue);
      json_object_set(volume, "acl", jacl);
      json_array_append(volumes, volume);
      json_decref(jvalue);
      json_decref(jacl);
  }
  else if(strcmp(key,"project") == 0) {
      json_t *jvalue = json_string(value);
      json_object_set(user, "project", jvalue);
      json_decref(jvalue);
  }

  //free(key);
  //free(value);
}


int drmaa_set_attribute(drmaa_job_template_t *jt, const char *name,
                        const char *value, char *error_diagnosis,
                        size_t error_diag_len) {
    if(strcmp(name, DRMAA_REMOTE_COMMAND) == 0 ) {
        json_t *command = json_object_get(jt->json, "command");
        json_t *jvalue = json_string(value);
        int err = json_object_set(command, "cmd", jvalue);
        json_decref(jvalue);
        //show_json(jt->json);
    }
    else if(strcmp(name, DRMAA_NATIVE_SPECIFICATION) == 0) {
        char *token;
        char *saveptr;
        char *nativespec = malloc(strlen(value)+1);
        strcpy(nativespec, value);
        //fprintf(stdout,"#NATIVE %s\n",nativespec);

        const char s[2] = ",";
        /* get the first token */
        token = strtok_r(nativespec, s, &saveptr);
        //fprintf(stdout,"#%s\n",token);
        set_native(jt->json, token);
        /* walk through other tokens */
        while( token != NULL )
        {
          token = strtok_r(NULL, s, &saveptr);
          if(token != NULL) {
              set_native(jt->json, token);
          }
        }
        free(nativespec);
    }
    return DRMAA_ERRNO_SUCCESS;
                        }

int drmaa_set_vector_attribute(drmaa_job_template_t *jt, const char *name,
                               const char *value[], char *error_diagnosis,
                               size_t error_diag_len) {
    int i = 0;
    if(strcmp(name, DRMAA_V_ARGV) == 0 ) {
        char *args = malloc(MAX_COMMAND_LINE);
        strcpy(args, "");
        while(value[i] != NULL) {
            strcat(args, " ");
            strcat(args, value[i]);
            i++;
        }
        json_t *command = json_object_get(jt->json, "command");
        const json_t* json_cmd = json_object_get(command, "cmd");
        const char *cmd = json_string_value(json_cmd);
        char *tmpcmd = malloc(MAX_COMMAND_LINE);
        strcpy(tmpcmd, cmd);
        strcat(tmpcmd, " ");
        strcat(tmpcmd, args);
        json_t *jvalue =  json_string(tmpcmd);
        int err = json_object_set(command, "cmd",jvalue);
        json_decref(jvalue);
        free(args);
        free(tmpcmd);
    }
    return DRMAA_ERRNO_SUCCESS;
}

/*
* drmaa_run_job() SHALL return DRMAA_ERRNO_SUCCESS on success, otherwise:
*    DRMAA_ERRNO_TRY_LATER,
*    DRMAA_ERRNO_DENIED_BY_DRM,
*    DRMAA_ERRNO_NO_MEMORY,
*    DRMAA_ERRNO_DRM_COMMUNICATION_FAILURE or
*    DRMAA_ERRNO_AUTH_FAILURE.
*/
int drmaa_run_job(char *job_id, size_t job_id_len,
                 const drmaa_job_template_t *jt, char *error_diagnosis,
                 size_t error_diag_len) {

    json_t* resp = api_post(jt->json, "/api/1.0/task");
    json_t* id = json_object_get(resp, "id");
    //show_json(resp);
    if(id != NULL){
        //strcpy(job_id, json_string_value(id));
        sprintf(job_id, "%d", (int)json_integer_value(id));
        vector_append(&job_vector, (int)json_integer_value(id));
        json_decref(resp);
        return DRMAA_ERRNO_SUCCESS;
    }
    else {
        json_decref(resp);
        return DRMAA_ERRNO_DENIED_BY_DRM;
    }

}

int drmaa_wait(const char *job_id, char *job_id_out, size_t job_id_out_len,
               int *stat, signed long timeout, drmaa_attr_values_t **rusage,
               char *error_diagnosis, size_t error_diag_len) {
    int job_id_len = strlen(job_id);
    const char* api_task = "/api/1.0/task/";
    char *url = malloc(strlen(api_task)+job_id_len+2);
    strcpy(url,api_task);
    strcat(url, job_id);
    int over = 0;
    long start = (long)time(NULL);
    long current = (long)time(NULL);
    int is_timeout= 0;
    while(over == 0) {
        if(timeout > -1 && current - start > timeout) {
            is_timeout = 1;
            break;
        }
        json_t* resp = api_get(config, url);
        if(resp == NULL) {
            error_diag_len = strlen(NOJOB);
            strncat(error_diagnosis, NOAUTH, error_diag_len);
            return DRMAA_ERRNO_INVALID_JOB;
        }
        json_t* status = json_object_get(resp, "status");
        json_t* primary = json_object_get(status, "primary");
        if(strcmp(json_string_value(primary),"over") == 0) {
            over = 1;
            json_t* container = json_object_get(resp, "container");
            json_t* meta = json_object_get(container, "meta");
            json_t* state = json_object_get(meta, "State");
            json_t* exitcode = json_object_get(state, "ExitCode");
            *stat = json_integer_value(exitcode);
            json_decref(exitcode);
            json_decref(state);
            json_decref(meta);
            json_decref(container);
            /*
            free(container);
            free(meta);
            free(state);
            free(exitcode);
            */

        }
        json_decref(status);
        json_decref(primary);
        json_object_clear(resp);
        json_decref(resp);
        //free(resp);
        //free(status);
        //free(primary);
        sleep(1);
        current = (long)time(NULL);
    }

    free(url);

    if(timeout == 1) {
        return DRMAA_ERRNO_EXIT_TIMEOUT;
    }

    return DRMAA_ERRNO_SUCCESS;
    //return DRMAA_ERRNO_NO_RUSAGE;
}

int drmaa_delete_job_template(drmaa_job_template_t *jt, char *error_diagnosis,
                              size_t error_diag_len) {
    json_object_clear(jt->json);
    json_decref(jt->json);
    free(jt);
    return DRMAA_ERRNO_SUCCESS;
}

int drmaa_wifsignaled(int *signaled, int stat, char *error_diagnosis,
                      size_t error_diag_len) {
    signaled = 0;
    return DRMAA_ERRNO_SUCCESS;
}

int drmaa_exit(char *error_diagnosis, size_t error_diag_len) {
    vector_free(&job_vector);
    json_object_clear(config);
    json_decref(config);
    return DRMAA_ERRNO_SUCCESS;
}


/*
 * Evaluates into 'exited' a non-zero value if stat was returned for a
 * job that terminated normally. A zero value can also indicate that
 * altough the job has terminated normally an exit status is not available
 * or that it is not known whether the job terminated normally. In both
 * cases drmaa_wexitstatus() SHALL NOT provide exit status information.
 * A non-zero 'exited' value indicates more detailed diagnosis can be provided
 * by means of drmaa_wifsignaled(), drmaa_wtermsig() and drmaa_wcoredump().
 */
int drmaa_wifexited(int *exited, int stat, char *error_diagnosis,
                    size_t error_diag_len) {
    if(stat > 0) {
        *exited = 1;
        return 0;
    }
    *exited = 0;
    return 0;
                    }

/*
 * If the OUT parameter 'exited' of drmaa_wifexited() is non-zero,
 * this function evaluates into 'exit_code' the exit code that the
 * job passed to _exit() (see exit(2)) or exit(3C), or the value that
 * the child process returned from main.
 */
int drmaa_wexitstatus(int *exit_status, int stat, char *error_diagnosis,
                      size_t error_diag_len) {
    *exit_status = stat;
    return 0;
                      }

/*
 * If the OUT parameter 'signaled' of drmaa_wifsignaled(stat) is
 * non-zero, this function evaluates into signal a string representation of the
 * signal that caused the termination of the job. For signals declared by POSIX,
 * the symbolic names SHALL be returned (e.g., SIGABRT, SIGALRM).
 * For signals not declared by POSIX, any other string MAY be returned.
 */
int drmaa_wtermsig(char *signal, size_t signal_len, int stat,
                   char *error_diagnosis, size_t error_diag_len) {
    signal = NULL;
    return 0;
    }


/*
 * If the OUT parameter 'signaled' of drmaa_wifsignaled(stat) is
 * non-zero, this function evaluates into 'core_dumped' a non-zero value
 * if a core image of the terminated job was created.
 */
int drmaa_wcoredump(int *core_dumped, int stat, char *error_diagnosis,
                    size_t error_diag_len) {
    *core_dumped = 0;
    return 0;
                    }

/*
 * Evaluates into 'aborted' a non-zero value if 'stat'
 * was returned for a job that ended before entering the running state.
 */
int drmaa_wifaborted(int *aborted, int stat, char *error_diagnosis,
                     size_t error_diag_len) {
    if(stat == -1) {
        *aborted = 1;
    }
    *aborted = 0;
    return 0;
    }

/*
 * Get the program status of the job identified by 'job_id'.
 * The possible values returned in 'remote_ps' and their meanings SHALL be:
 *
 * DRMAA_PS_UNDETERMINED          = 0x00: process status cannot be determined
 * DRMAA_PS_QUEUED_ACTIVE         = 0x10: job is queued and active
 * DRMAA_PS_SYSTEM_ON_HOLD        = 0x11: job is queued and in system hold
 * DRMAA_PS_USER_ON_HOLD          = 0x12: job is queued and in user hold
 * DRMAA_PS_USER_SYSTEM_ON_HOLD   = 0x13: job is queued and in user and system
 *                                        hold
 * DRMAA_PS_RUNNING               = 0x20: job is running
 * DRMAA_PS_SYSTEM_SUSPENDED      = 0x21: job is system suspended
 * DRMAA_PS_USER_SUSPENDED        = 0x22: job is user suspended
 * DRMAA_PS_USER_SYSTEM_SUSPENDED = 0x23: job is user and system suspended
 * DRMAA_PS_DONE                  = 0x30: job finished normally
 * DRMAA_PS_FAILED                = 0x40: job finished, but failed
 *
 * DRMAA SHOULD always get the status of job_id from DRM system, unless the
 * previous status has been DRMAA_PS_FAILED or DRMAA_PS_DONE and the status has
 * been successfully cached. Terminated jobs get DRMAA_PS_FAILED status.
 *
 * drmaa_synchronize() SHALL return DRMAA_ERRNO_SUCCESS on success, otherwise:
 *    DRMAA_ERRNO_DRM_COMMUNICATION_FAILURE,
 *    DRMAA_ERRNO_AUTH_FAILURE,
 *    DRMAA_ERRNO_NO_MEMORY or
 *    DRMAA_ERRNO_INVALID_JOB.
 */
int drmaa_job_ps(const char *job_id, int *remote_ps, char *error_diagnosis,
                 size_t error_diag_len) {
    int job_id_len = strlen(job_id);
    const char* api_task = "/api/1.0/task/";
    char *url = malloc(strlen(api_task)+job_id_len+2);
    strcpy(url,api_task);
    strcat(url, job_id);

    json_t* resp = api_get(config, url);
    if(resp == NULL) {
        error_diag_len = strlen(NOJOB);
        strncat(error_diagnosis, NOAUTH, error_diag_len);
        return DRMAA_ERRNO_INVALID_JOB;
    }
    json_t* status = json_object_get(resp, "status");
    json_t* primary = json_object_get(status, "primary");
    json_t* secondary = json_object_get(status, "secondary");
    if(strcmp(json_string_value(primary),"pending") == 0) {
        *remote_ps = DRMAA_PS_QUEUED_ACTIVE;
    }
    else if(strcmp(json_string_value(primary),"running") == 0) {
        if(strcmp(json_string_value(secondary),"suspended") == 0) {
            *remote_ps = DRMAA_PS_USER_SUSPENDED;
        }
        else {
            *remote_ps = DRMAA_PS_RUNNING;
        }
    }
    else if(strcmp(json_string_value(primary),"over") == 0) {
            json_t* container = json_object_get(resp, "container");
            json_t* meta = json_object_get(container, "meta");
            json_t* state = json_object_get(meta, "State");
            json_t* exitcode = json_object_get(state, "ExitCode");
            int stat = json_integer_value(exitcode);
            json_decref(container);
            json_decref(meta);
            json_decref(state);
            json_decref(exitcode);
            if(stat > 0){
                *remote_ps = DRMAA_PS_FAILED;
            }
            else {
                *remote_ps = DRMAA_PS_DONE;
            }
    }

    //free(resp);
    //free(status);
    //free(primary);
    json_object_clear(resp);
    json_decref(resp);
    free(url);


    return DRMAA_ERRNO_SUCCESS;
    }


/*
 * SHALL return the error message text associated with the errno number. The
 * routine SHALL return null string if called with invalid ERRNO number.
 */
const char *drmaa_strerror(int drmaa_errno){
    //TODO not yet implemented
    return NULL;
}


/*
 * If called before drmaa_init(), it SHALL return a comma delimited DRM systems
 * string, one per each DRM system provided implementation. If called after
 * drmaa_init(), it SHALL return the selected DRM system. The output string is
 * implementation dependent.
 *
 * drmaa_get_DRM_system() SHALL return DRMAA_ERRNO_SUCCESS on success,
 * otherwise:
 *    DRMAA_ERRNO_INTERNAL_ERROR.
 */
int drmaa_get_DRM_system(char *drm_system, size_t drm_system_len,
         char *error_diagnosis, size_t error_diag_len) {
             drm_system = "GODOCKER";
             return DRMAA_ERRNO_SUCCESS;
         }


/*
 * If called before drmaa_init(), it SHALL return a comma delimited DRMAA
 * implementations string, one per each DRM system provided implementation. If
 * called after drmaa_init(), it SHALL return the selected DRMAA implementation.
 * The output (string) is implementation dependent. drmaa_get_DRM_implementation
 * routine SHALL return DRMAA_ERRNO_SUCCESS on success, otherwise:
 *    DRMAA_ERRNO_INTERNAL_ERROR.
 */
int drmaa_get_DRMAA_implementation(char *drmaa_impl, size_t drmaa_impl_len,
         char *error_diagnosis, size_t error_diag_len) {
             drmaa_impl = "GODOCKER";
             return DRMAA_ERRNO_SUCCESS;
         }




int drmaa_synchronize(const char *job_ids[], signed long timeout, int dispose,
                      char *error_diagnosis, size_t error_diag_len) {
    //TODO not yet implemented
    int i;
    int sync_all = 0;
    Vector jobs;
    vector_init(&jobs);
    if(strcmp(job_ids[0], DRMAA_JOB_IDS_SESSION_ALL) == 0) {
        for(i=0;i<job_vector.size;i++){
            vector_append(&jobs, job_vector.data[i]);
        }
    }
    else {
        for (i=0; job_ids[i] != NULL; i++) {
            vector_append(&jobs, atoi(job_ids[i]));
        }
    }

    int nb_jobs_over = 0;

    long start = (long)time(NULL);
    long current = (long)time(NULL);
    int is_timeout= 0;
    while(nb_jobs_over != jobs.size) {
        if(timeout > -1 && current - start > timeout) {
            is_timeout = 1;
            break;
        }
        for(i=0;i<jobs.size;i++){
            int job_id =  vector_get(&jobs, i);
            if(job_id>=0) {
                char job_id_str[10];
                snprintf(job_id_str, 10, "%d", job_id);
                int job_id_len = sizeof(job_id_str);
                char *url = malloc(sizeof("/api/1.0/task/")+job_id_len+1);
                strcpy(url,"/api/1.0/task/");
                strcat(url, job_id_str);
                fprintf(stdout,"-- Check %d\n", job_id);
                json_t* resp = api_get(config, url);
                if(resp == NULL) {
                    error_diag_len = strlen(NOJOB);
                    strncat(error_diagnosis, NOAUTH, error_diag_len);
                    return DRMAA_ERRNO_INVALID_JOB;
                }
                json_t* status = json_object_get(resp, "status");
                json_t* primary = json_object_get(status, "primary");
                if(strcmp(json_string_value(primary),"over") == 0) {
                    fprintf(stdout,"-- Over %d\n", job_id);
                    vector_set(&jobs, i, -1);
                    nb_jobs_over++;
                }
                free(url);
                //json_decref(primary);
                //json_decref(status);
                json_object_clear(resp);
                json_decref(resp);
                //free(resp);
                //free(status);
                //free(primary);
            }
        }
        sleep(1);
        current = (long)time(NULL);

    }


    vector_free(&jobs);

    vector_clear(&job_vector);

    return DRMAA_ERRNO_SUCCESS;
    }



/*
 * If 'name' is an existing non-vector attribute name in the job
 * template 'jt', then the value of 'name' SHALL be returned; otherwise,
 * NULL is returned.
 *
 * drmaa_get_attribute() SHALL return DRMAA_ERRNO_SUCCESS on success, otherwise:
 *    DRMAA_ERRNO_INVALID_ATTRIBUTE_VALUE.
 */
int drmaa_get_attribute(drmaa_job_template_t *jt, const char *name, char *value,
                        size_t value_len, char *error_diagnosis,
                        size_t error_diag_len) {
    //TODO
    printf("drmaa_get_attribute\n");
    return DRMAA_ERRNO_SUCCESS;
    }
int drmaa_get_next_attr_name(drmaa_attr_names_t* values, char *value,
                             size_t value_len) {
    //TODO
    printf("drmaa_get_next_attr_name\n");
    //return DRMAA_ERRNO_SUCCESS;
    return DRMAA_ERRNO_NO_MORE_ELEMENTS;
                             }
int drmaa_get_next_attr_value(drmaa_attr_values_t* values, char *value,
                              size_t value_len){
     return DRMAA_ERRNO_NO_MORE_ELEMENTS;
                              }
int drmaa_get_next_job_id(drmaa_job_ids_t* values, char *value,
                          size_t value_len){
    //TODO
    printf("drmaa_get_next_job_id\n");
    //return DRMAA_ERRNO_SUCCESS;
    return DRMAA_ERRNO_NO_MORE_ELEMENTS;
                          }

int drmaa_get_num_attr_names(drmaa_attr_names_t* values, int *size){
//TODO
printf("drmaa_get_num_attr_names\n");
return DRMAA_ERRNO_SUCCESS;
}
int drmaa_get_num_attr_values(drmaa_attr_values_t* values, int *size){
//TODO
printf("drmaa_get_num_attr_values\n");
return DRMAA_ERRNO_SUCCESS;
}
int drmaa_get_num_job_ids(drmaa_job_ids_t* values, int *size){
//TODO
printf("drmaa_get_num_job_ids\n");
return DRMAA_ERRNO_SUCCESS;
}
void drmaa_release_attr_names(drmaa_attr_names_t* values){
//TODO
printf("drmaa_release_attr_names\n");
}
void drmaa_release_attr_values(drmaa_attr_values_t* values){
//TODO
printf("drmaa_release_attr_values\n");
}
void drmaa_release_job_ids(drmaa_job_ids_t* values){
//TODO
printf("drmaa_release_job_ids\n");
}
int drmaa_get_vector_attribute(drmaa_job_template_t *jt, const char *name,
                               drmaa_attr_values_t **values,
                               char *error_diagnosis, size_t error_diag_len){
      //TODO
      printf("drmaa_get_vector_attribute\n");
      return DRMAA_ERRNO_SUCCESS;
                               }
int drmaa_get_attribute_names(drmaa_attr_names_t **values,
                              char *error_diagnosis, size_t error_diag_len){
     //TODO
     printf("drmaa_get_attribute_names\n");
     return DRMAA_ERRNO_SUCCESS;
                              }
int drmaa_get_vector_attribute_names(drmaa_attr_names_t **values,
                                     char *error_diagnosis,
                                     size_t error_diag_len){
            //TODO
            printf("drmaa_get_vector_attribute_names\n");
            return DRMAA_ERRNO_SUCCESS;
                                     }
int drmaa_run_bulk_jobs(drmaa_job_ids_t **jobids,
                        const drmaa_job_template_t *jt, int start, int end,
                        int incr, char *error_diagnosis, size_t error_diag_len){
//TODO
printf("drmaa_run_bulk_jobs\n");
return DRMAA_ERRNO_SUCCESS;
                        }
int drmaa_control(const char *jobid, int action, char *error_diagnosis,
                  size_t error_diag_len){
//TODO
printf("drmaa_control\n");
return DRMAA_ERRNO_SUCCESS;
                  }
